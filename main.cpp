#include <iostream>
#include <SDL2/SDL.h>
#include <eigen3/Eigen/Dense>
#include <list>
#include <array>
#include <algorithm>
#include <list>
#include <vector>
#include <math.h>
#include "Ply.h"

using namespace Eigen;
using namespace std;

Vector2i projectPoint(const Matrix4d &worldToCameraMatrix, const Vector4d &point)
{
    // Compute point from camera
    auto pointCamera = worldToCameraMatrix * point;
//    cout << pointCamera.x() << "," << pointCamera.y() << "," << pointCamera.z() << endl;
    // Compute perspective projection
    auto screenPoint = Vector2d(pointCamera.x() / -pointCamera.z(), pointCamera.y() / -pointCamera.z());
    // Normalize respect to canvas size
    auto ndcPoint = Vector2d((screenPoint.x() + 2 * .5) / 2, (screenPoint.y() + 2 * .5) / 2);
    // Rasterize
    auto rasterPoint = Vector2i(ndcPoint.x() * 600, (1 - ndcPoint.y()) * 600);

    return rasterPoint;
}

void draw(SDL_Renderer *&renderer, const std::vector<Vector2i> & vertices, uint_fast16_t i, uint_fast16_t j)
{
    SDL_RenderDrawLine(renderer, vertices.at(i).x(), vertices.at(i).y(), vertices.at(j).x(), vertices.at(j).y());
}


int SDL_main(int argc, char * args[]) {


    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cout << "Error on init SDL : " << SDL_GetError() << std::endl;
        return -1;
    }



    SDL_Window *window;
    SDL_Renderer *renderer;

    if (SDL_CreateWindowAndRenderer(600, 600, 0, &window, &renderer) != 0)
    {
        std::cout << "Error when creating renderer" << std::endl;
        return -1;
    }
    SDL_bool done = SDL_FALSE;

    uint32_t i = SDL_GetTicks();
    uint32_t totalDelta = 0;

    // Cube vertices coordinates
//    std::array<Vector4d, 8> vertices =
//    {
//        Vector4d(-0.5,-0.5,0.5, 1.),
//        Vector4d(0.5,-0.5,0.5, 1.),
//        Vector4d(-0.5,0.5,0.5, 1.),
//        Vector4d(0.5,0.5,0.5, 1.),
//
//        Vector4d(-0.5,-0.5,-0.5, 1.),
//        Vector4d(0.5,-0.5,-0.5, 1.),
//        Vector4d(-0.5,0.5,-0.5, 1.),
//        Vector4d(0.5,0.5,-0.5, 1.),
//    };

    // Ply vertices coordinates
    Ply * p = new Ply();
    //std::vector<Vector4d> vertices = p->load("D:\\Projects\\Showcases\\rasterize_c\\rabbit.ply");
    std::vector<tuple<Vector4d, Vector3d>> data = p->load("D:\\Projects\\Showcases\\rasterize_c\\rabbit.ply");
    std::vector<Vector4d> vertices;
    std::vector<Vector3d> normals;
    std::transform(data.begin(), data.end(), std::back_inserter(vertices), [](auto d) { return std::get<0>(d); });
    std::transform(data.begin(), data.end(), std::back_inserter(normals), [](auto d) { return std::get<1>(d); });


    Matrix4d  cameraToWorldMatrix;
    cameraToWorldMatrix <<  1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1;
    //cameraToWorldMatrix << 0.718762, 0.615033, -0.324214, 0, -0.393732, 0.744416, 0.539277, 0, 0.573024, -0.259959, 0.777216, 0, 0.526967, 1.254234, -2.53215, 1;

    auto worldToCameraMatrix = cameraToWorldMatrix.inverse();




    const int_fast32_t FRAMERATE = 1000 / 120;
    double theta { 0 };
    double gamma { 0 };

    while (!done) {

        uint_fast32_t delta = i - SDL_GetTicks();
        totalDelta += delta;

        if (totalDelta < FRAMERATE)
        {
            continue;
        }
        totalDelta = 0;

        SDL_Event event;

        SDL_SetRenderDrawColor(renderer, 20, 20, 25, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);


        Matrix4d cubeTransform;
        theta+=0.01;
        gamma+=0.001;


        Matrix4d cubeToWorld;
        cubeToWorld <<
            1,0,0,0,
            0,1,0,0,
            0,0,1,3,
            0,0,0,1;

//        cubeTransform <<
//            cos(theta) * cos(gamma),-sin(theta) * cos(gamma),sin(gamma),0,
//                sin(theta),cos(theta),0,0,
//                cos(theta) * -sin(gamma),-sin(theta) * -sin(gamma),cos(gamma),0.5,
//                0,0,0,1;

        cubeTransform <<
                      cos(theta),0,-sin(theta),0,
                      0,1,0,0,
                    -sin(theta),0, cos(theta),0.5,
                0,0,0,1;


        std::vector<Vector4d> cubeVertices;
        std::transform(vertices.begin(), vertices.end(), std::back_inserter(cubeVertices), [cubeToWorld, cubeTransform](auto p) { return cubeTransform * p; });

//        cout << "cube vertices" << endl;

        for (const auto &vertice : cubeVertices)
        {
//            cout << vertice.x() << "," << vertice.y() << "," << vertice.z() << endl;
        }

//        cout << "end" << endl;


        std::vector<Vector2i> projectedVertices;
        std::transform(cubeVertices.begin(), cubeVertices.end(), std::back_inserter(projectedVertices), [worldToCameraMatrix](auto p) { return projectPoint(worldToCameraMatrix, p); });

        //cout << "Projected vertices: " << projectedVertices.size() << endl;

        int i = 0;
        for (const auto &vertice : projectedVertices)
        {
            double r = (normals.at(i).x() + 1.) * 0.5 * 255.;
            double g = (normals.at(i).y() + 1.) * 0.5 * 255.;
            double b = (normals.at(i).z() + 1.) * 0.5 * 255.;

            SDL_SetRenderDrawColor(renderer, r, g, b, 127);
            SDL_RenderDrawPoint(renderer, vertice.x(), vertice.y());
//            cout << vertice.x() << "," << vertice.y() << endl;
            i++;
        }

//        draw(renderer, projectedVertices, 0, 1);
//        draw(renderer, projectedVertices, 1, 3);
//        draw(renderer, projectedVertices, 3, 2);
//        draw(renderer, projectedVertices, 2, 0);
//
//        draw(renderer, projectedVertices, 4, 5);
//        draw(renderer, projectedVertices, 5, 7);
//        draw(renderer, projectedVertices, 7, 6);
//        draw(renderer, projectedVertices, 6, 4);
//
//        draw(renderer, projectedVertices, 0, 4);
//        draw(renderer, projectedVertices, 2, 6);
//        draw(renderer, projectedVertices, 1, 5);
//        draw(renderer, projectedVertices, 3, 7);
//
//        SDL_SetRenderDrawColor(renderer, 0, 125, 97, 127);
//
//        draw(renderer, projectedVertices, 0, 3);
//        draw(renderer, projectedVertices, 4, 7);
//        draw(renderer, projectedVertices, 0, 6);
//        draw(renderer, projectedVertices, 5, 3);
//        draw(renderer, projectedVertices, 0, 5);
//        draw(renderer, projectedVertices, 2, 7);




        SDL_RenderPresent(renderer);

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                done = SDL_TRUE;
            }
        }

        i = SDL_GetTicks();
    }


    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}

