//
// Created by me on 6/24/2023.
//

#ifndef PERSPECTIVEPROJECTION_PLY_H
#define PERSPECTIVEPROJECTION_PLY_H


#include <vector>
#include <string>
#include <eigen3/Eigen/Core>

using namespace Eigen;
using namespace std;

class Ply {
public:
    vector<tuple<Vector4d, Vector3d>> load(string filename);
};


#endif //PERSPECTIVEPROJECTION_PLY_H
