//
// Created by me on 6/24/2023.
//

#include "Ply.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;

vector<string> split(string str, char delimiter) {
    std::replace(str.begin(), str.end(), delimiter, ' ');  // replace ':' by ' '

    vector<string> array;
    stringstream ss(str);
    string temp;
    while (ss >> temp)
        array.push_back(temp);

    return array;
}

std::vector<tuple<Vector4d, Vector3d>> Ply::load(std::string filename) {
    std::vector<tuple<Vector4d, Vector3d>> result;

    std::string line;
    std::ifstream f (filename);
    if (f.is_open())
    {
        int i = 0;
        int vertexStartIdx = -1;
        int vertexEndIdx = -1;
        int nVertex = -1;
        std::vector<string> v;
        while ( getline (f, line) )
        {
            if (line.rfind("element vertex", 0) == 0) {
                nVertex = stoi(split(line, ' ').at(2));
            }
            if (line.rfind("end_header", 0) == 0) {
                vertexStartIdx = i;
                vertexEndIdx = vertexStartIdx + nVertex;
            }

            if (vertexStartIdx != -1 && i > vertexStartIdx && i < vertexEndIdx + 1) {
                std::vector<int> points;
                std::vector<string> strPoints = split(line, ' ');
                //std::transform(strPoints.begin(), strPoints.end(), std::back_inserter(points), [](auto x) { return stoi(x); });
                //result.push_back(points);
                //v.push_back(line);
//                result.push_back(Vector4d(stod(strPoints.at(0)), stod(strPoints.at(1)), stod(strPoints.at(2)), 1.));
                result.push_back(tuple<Vector4d, Vector3d>(Vector4d(stod(strPoints.at(0)), stod(strPoints.at(1)), stod(strPoints.at(2)), 1.), Vector3d(stod(strPoints.at(3)),stod(strPoints.at(4)),stod(strPoints.at(5)))));
            }

            i++;
        }
        f.close();
        cout << "n vertex: " << nVertex;
        cout << "n " << v.size();
        //for_each(v.begin(), v.end(), [](string x) { cout << x; });
    }

    else cout << "Unable to open file";

    return result;
}